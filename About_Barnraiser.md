## About Barnraiser

All projects in this repository were published at http://barnraiser.org/ a while ago. Although they had very promising features they were frozen and finally dropped for reasons which are explained [here](http://barnraiser.org/signing_off). According to this the main reason was the rise of similar products by companies with sufficient manpower and strategies to attract more users than a small enthusiastic bunch of coders.

Today we know where that evolution brought us and theres a tendency to move away from data-miners back to software under personal control. Funny enough this was exactly the intention of all projects at barnraiser.org. We could say they were just a bit too far ahead of their time.

Fortunately they left their open source code available on their website. In order to preserve it and with the hope that some experienced coders will step in and adjust it to modern requirements I took the freedom to import all of their projects into this repository.

As I'm not the most experienced code-writer I will rather take care of organisational matters in the background. Everybody who is able and willing to spend some time on these projects is invited to join this organisation. I also wouldn't mind if you prefer to fork and develop on your own, but then I would appreciate pull requests in order to keep things together.
# Barnraiser-Roundhouse

## About Barnraiser

You can read more about the background of this repository here: [About_Barnraiser](About_Barnraiser.md)

This repository is based upon the latest version roundhouse_20080929.tar.gz found at http://barnraiser.org/roundhouse.

The following is the original description of *Roundhouse*, taken from its homepage http://barnraiser.org/roundhouse

---

## Introducing Roundhouse
Import blog entries from the whole blogsphere into your blog stream with Roundhouse; our social blogging tool.

Roundhouse is a lightweight easy to learn and use blogging tool. You can install it for just yourself or for many people to each have their own blog.

Roundhouse was built as an experiment to see if we can incorporate a blogroll into the main blog feed in a way that you could gather blog entries from friends into your blog to create a combination of your blog entries and your favourite blog entries from your friends to create a social blog.

## Features
* Install as a single blog or a service to host many separate blogs.
* OpenID support.
* Import RSS feed items directly into your blog.
* Import Digg items directly into your blog.
* Upload many pictures for inclusion in your blog.
* Import Youtube movies into your blog.
* Blog archive built in.
* Tagcloud built in.
* RSS feed built in.
* Highlights listing.
* Receive comments on blogs.
* Share blog entries (to del.icio.us, Digg, StumbleUpon and Technorati).
* Optimized for Google indexing.
* Themed "skins" which can be easily downloaded and added.
* Multi-lingual.
* Easy to use publishing system.
* Lightweight easy to use interface.
* Free (GPL) software license

## Technical considerations
Roundhouse requires a web server running either Apache 1.3/2.x or IIS5/IIS6 with PHP5.x installed including GD library and Gettext (Curl and BCMath if you want OpenID support).

For multiple instances you will require wildcard sub-domains.

---

There's also a very comprehensive user's manual available at http://barnraiser.org/roundhouse_guide which I copied
[here](documents/roundhouse_guide.html) for backup purposes.